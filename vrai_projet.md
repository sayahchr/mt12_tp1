
# Fonction gamma
## Q1
$$ \Gamma(x)	= \int_0^{+\infty} t^{x-1}e^{-t}dt = \int_0^{+\infty} f(t,x)dt $$ est-elle définie sur $]0, +\infty[$?

On fixe $x \in \mathbb{R}^*_+$. On notera $f(t)$.
Pour t proche de 0: $t^{x-1} e^{-t}\sim t^{x-1}\underset{t\to 0}{\longrightarrow} k\in\mathbb{R}$ car $x-1>-1$

Un théorème de croissance comparée donne:
$$
\lim\limits_{x \to +\infty} \frac{e^{ax}}{x^b} =+\infty \implies \lim\limits_{x \to +\infty} x^be^{-ax} =0
$$
D'où $\lim\limits_{t \to +\infty}{f(t)} =0$
Sur $]0, A[, A<+\infty$, on a f continue et positive.
Finalement, on a $f(t)$ continue positive sur tout $]0,+\infty[$.
Cela est vrai pour tout $x \in ]0, +\infty[$.
## Q2
$$ \frac{d}{dx}\left(e^{-t}t^{x-1}\right) = e^{-t}t^{x-1}log t$$
D'après la règle de Leibniz, il faut montrer qu'il existe g intégrable sur t telle que:
$$
\mid e^{-t}t^{x-1}logt\mid \le g
$$
En prenant $g(t)=\mid e^{-t}t^{x-1}log t\mid = (-logt)e^{-t}t^{x-1}\mathbb{1}_{]0,1[}(t) + log(t)e^{-t}t^{x-1}\mathbb{1}_{[1, +\infty[}(t)$
Étudions l'intégrabilité de g sur t:
$\lim\limits_{1-} g(t) =0=\lim\limits_{1+} g(t)$
Sur $]0,1[$ et sur $]1,+\infty[$ et en 1, g est continue donc intégrable.
Prenons $t \lt 1 \implies (-log t)-\mid -logt\mid$
En 0, on a $e^{-t} \sim 1$ et les théorèmes de croissance comparée donnent $\lim\limits_{t \rightarrow 0^+} \mid -logt \mid t^{x-1}=0$ donc on a intégrabilité en 0.
$log(t) = o(t)$ et $t^{x-1}=o(e^{t/2}) \implies log(t)e^{-t}t^{x-1}=o(e^{-t/2})$
d'où intégrabilité

On a bien intégrabilité de g, donc on a le droit d'écrire $\Gamma^{'}(x) = \int_0^{+\infty} log(t)e^{-t}t^{x-1}dt$


Avec le même raisonnement, on peut écrire $\Gamma^{(k)}$ par réccurrence.

Supposons que $\Gamma$ est $C^k$, et que $\Gamma^{(k)} = \int_0^{+\infty}(log(t))^k e^{-t}t^{x-1}dt$.

Alors il suffit de montrer que $\mid e^{-t}t^{x-1}(logt)^k\mid$ intégrable. On utilise la même idée de démonstration qu'en Q2 donc on détaillera moins.

Si k impair:
   
$$ \mid e^{-t}t^{x-1}(logt)^k\mid = (-logt)^k e^{-t}—t^{x-1}\mathbb{1}_{]0,1[}(t) + (logt)^k e^{-t} \mathbb{1}_{[1, +\infty[}(t) $$

Si k pair:

$$ \mid e^{-t}t^{x-1}(logt)^k\mid = (logt)^k e^{-t}t^{x-1} $$

Montrons l'intégrabilité de g par rapport à t:

### En 1

$\lim\limits_{t \to 1^+}{g(t)} =0 =\lim\limits_{t \to 1^-}{g(t)}$

g continue donc intégrable.

### sur $]0, A[, A<+\infty$

g continue car composée de fonctions usuelles connues comme continues, donc intégrable

### en 0

On utilise les mêmes théorèmes de croissance comparée qu'en Q2 et on trouve:
$$
\lim\limits_{t \to 0^+}{\mid -logt \mid^k t^{x-1}} = 0
$$
et $e^{-t}\sim 1$
$\implies$ intégrabilité de $-(logt)^k t^{x-1} e^{-t}$ en 0.

### En $+\infty$

$logt = o(-k/2)$ et $t^{x-1}=o(e^{t/2})$ d'où $g(t)=o(e^{t/2})$ d'où intégrabilité.

Finalement g intégrable donc la règle de leibniz nous permet de dériver sous la courbe, donc $\Gamma^{k+1} = \int_0^{+\infty}(logt)^ke^{-t}t^{x-1}dt$ ce qui achève la récurrence: on a montré que $\Gamma$ est $C^{(k+1)}$. Donc par récurrence $\Gamma$ est $C^{\infty}$.
 
### Q3

On fait une intégration par parties:

$$\Gamma(x) = \int_0^{+\infty}e^{-t}t^{x-1}dt - \left[e^{-t}\frac{t^x}{x}\right]_0^{+\infty} + \frac{1}{x}\int_0^{+\infty}e^{-t}t^x dt = 0 + \frac{1}{x}\Gamma(x-1)$$

$$\implies \Gamma(x+1)=x\Gamma(x)$$

Montrons par récurrence que $\Gamma(n)=(n-1)!$:

* initialisation: $\Gamma(1) = \int_0^{+\infty}e^{-t}dt = 1$
* hérédité:

supposons que $\Gamma(n) = (n-1)!$, il s'agit de montrer que $\Gamma(n+1) = n! = n(n-1)!$

or on a montré plus haut que $\Gamma(n+1) = n\Gamma(n) = n(n-1)!$ CQFD.

## Q4: calcul numérique
Le pas demandé est 0,5. Avec l'expression $\Gamma(n)=(n-1)!$ on peut déjà calculer exactement les points $(n, \Gamma(n)), n\in \mathbb{N}$. Il reste à calculer les points $(x, \Gamma(x)), n<x<n+1, n\in\mathbb{N}$. On approche la courbe en utilisant $\Gamma^{'} et en laissant scilab calculer une valeur approchée de l'intégrale.

	--> function y=gammaEntier(n)
	  > y = factorial(n-1)
	  > endfunction

	--> function y = gammap(x)
	  > y = integrate('log(t)*exp(-t)*t^(x-1)', 't', 0, 10000)
	  > endfunction
10000 est un nombre très grand approchant $+\infty$. On commet une erreur.

	--> function y = gammaIntermediaire(x)
	  > y = gammaEntier(floor(x)) + (floor(x)-x).*matrixfun(gammap,x)
	  > endfunction
On a réalisé une approximation de la courbe par la tangente calculée en chaque n.
Voici les graphiques. Les valeurs exactes de gamma calculées sur $\mathbb{N}$:

	--> N=[1:9]
	--> plot(N,gammaEntier(N), 'o')

![Calcul de gamma sur {0,..,9}](https://gitlab.utc.fr/sayahchr/mt12_tp1/raw/master/gamma_scilab_1.svg)

	--> X=[1:0.5:9]
	--> plot(X, gammaIntermediaire(X))

![Approximation de gamma par la tangente et avec un pas de 0.5](https://gitlab.utc.fr/sayahchr/mt12_tp1/raw/master/gamma_scilab_2.svg)

# Fonction bêta

## Q1
L'intégrande est continu sur ]0,1[ et équivalent à  $t^{x-1}$en 0 et à	$(1-tcos(\theta)sin(\theta) {d\theta}
\\
=2 \times \int _{0}^{\frac{\pi}{ 2} }sin(\theta)^{2a-2} cos(\theta)^{2b-2}cos(\theta)sin(\theta) {d\theta}
\\
=2 \times \int _{0}^{\frac{\pi}{ 2} }sin(\theta)^{2a-1} sin(\theta)^{-1} cos(\theta)^{2b-1}cos(\theta)^{-1}cos(\theta)sin(\theta) {d\theta}
\\
=2 \times \int _{0}^{\frac{\pi}{ 2} }sin(\theta)^{2a-1}  cos(\theta)^{y2b-1}$  en 1, donc l'intégrale converge si et seulement si $x,y>0$/
{d\theta}
$$
## Question 2

 - $t=sin^2(\theta)$
   
- $\frac{dt}{d\theta}=2 \times  cos(\theta)sin(\theta)$ 
 $dt =2 \times cos(\theta)sin(\theta) {d\theta}$
   
 - $\theta  = arcsin(\sqrt{t})$

$arcsin(\sqrt{0})=1$
 $arcsin(\sqrt{1})=\frac{\pi}{2}$
 $0 \leq \theta \leq \frac{\pi}{2}$

 


$$
B(a,b)=\int _{0}^{1 }t^{a-1} (1-t)^{b-1}dt
\\
=\int _{0}^{\frac{\pi}{ 2} }sin^2(\theta)^{a-1} (1-sin^2(\theta))^{b-1} \times 2 \times cos(\theta)sin(\theta) {d\theta}
\\
=2 \times \int _{0}^{\frac{\pi}{ 2} }sin^2(\theta)^{a-1} cos^2(\theta)^{b-1}cos(\theta)sin(\theta) {d\theta}
\\
=2 \times \int _{0}^{\frac{\pi}{ 2} }sin(\theta)^{2a-2} cos(\theta)^{2b-2}cos(\theta)sin(\theta) {d\theta}
\\
=2 \times \int _{0}^{\frac{\pi}{ 2} }sin(\theta)^{2a-1} sin(\theta)^{-1} cos(\theta)^{2b-1}cos(\theta)^{-1}cos(\theta)sin(\theta) {d\theta}
\\
=2 \times \int _{0}^{\frac{\pi}{ 2} }sin(\theta)^{2a-1}  cos(\theta)^{2b-1}{d\theta}
$$

## Question 3



$$\displaystyle \Gamma (a)=\int _{0}^{+\infty }t_a^{a-1}\operatorname {e} ^{-t_a}\mathrm {d} t_a
$$
Soit le changement de variable suivant  : 
$$ 
{\displaystyle t_a=s_a^2} 
\\
\frac{\mathrm {d}t_a}{\mathrm {d}s_a}=2s_a 
\\
\\
\mathrm {d}t_a=2s_a \mathrm {d}s_a  $$
$$
\displaystyle \Gamma (a)=\int _{0}^{+\infty }(s_a^2)^{a-1}\operatorname {e} ^{-s_a^2}2s_a\mathrm {d} s_a$$

Faisons la même chose pour $\displaystyle \Gamma (b)$
$$
{\displaystyle \Gamma (a)\,\Gamma (b)=\int _{0}^{+\infty }(s^2_a)^{a-1}\operatorname {e} ^{-s_a^2}2s_a\mathrm {d} s_a\int _{0}^{+\infty }(s_b^2)^{b-1}\operatorname {e} ^{-s_b^2} 2 s_b\mathrm {d} s_b}
\\
{\displaystyle \Gamma (a)\,\Gamma (b)=\int _{0}^{+\infty }s_a^{2a-2}\operatorname {e} ^{-s_a^2}2s_a\mathrm {d} s_a\int _{0}^{+\infty }s_b^{2b-2}\operatorname {e} ^{-s_b^2} 2 s_b\mathrm {d} s_b}
\\
{\displaystyle \Gamma (a)\,\Gamma (b)=\int _{0}^{+\infty }s_a^{2a-1}s_a^{-1}\operatorname {e} ^{-s_a^2}2s_a\mathrm {d} s_a\int _{0}^{+\infty }s_b^{2b-1}s_b^{-1}\operatorname {e} ^{-s_b^2} 2 s_b\mathrm {d} s_b}
\\
{\displaystyle \Gamma (a)\,\Gamma (b)=2\int _{0}^{+\infty }s_a^{2a-1}\operatorname {e} ^{-s_a^2}\mathrm {d} s_a  \ 2\int _{0}^{+\infty }s_b^{2b-1}\operatorname {e} ^{-s_b^2} \mathrm {d} s_b}
\\
{\displaystyle \Gamma (a)\,\Gamma (b)=4\int _{0}^{+\infty }s_a^{2a-1}\operatorname {e} ^{-s_a^2}\mathrm {d} s_a  \ \int _{0}^{+\infty }s_b^{2b-1}\operatorname {e} ^{-s_b^2} \mathrm {d} s_b}
\\
=4\int _{0}^{+\infty }\int _{0}^{+\infty }s_a^{2a-1}s_b^{2b-1}\operatorname {e} ^{-(s_a^2+s_b^2 )}\mathrm {d} s_a\mathrm {d} s_b
$$
Soit le deuxième changement de variables $s_a=rcos\theta$ 
$s_b=rsin\theta$
On a donc  : 
$\mathrm {d} s_a\mathrm {d} s_b=r\mathrm {d} r\mathrm {d} \theta$
$$
\displaystyle \Gamma (a)\,\Gamma (b)=4\int _{0}^{\frac{\pi}{2}}\int _{0}^{+\infty }r^{2a+2b-1}cos(\theta)^{2a-1}sin(\theta)^{2b-1} r\mathrm {d} r\mathrm {d} \theta
\\
=[2\int _{0}^{\frac{\pi}{2}}cos(\theta)^{2a-1}sin(\theta)^{2b-1}  \theta \mathrm {d} \theta][2\int _{0}^{+\infty }r^{2(a+b)-1} \operatorname {e} ^{-r^2}\mathrm {d} r]
\\
=B(a,b)\displaystyle \Gamma (a+b)
$$


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTkxNDYwOTA3NywtOTcwODc1Njc1LC00Mj
U1MzA2ODksNjM3NjEzMzU3LC0xMDQxNDY3NzcyLC0xNDI2OTI2
Nzk0LC0xOTU5Nzg4NjI5LDEwNzAyMTYzMTksLTE2MDUzMzE1Nj
gsLTE4MDUxNDIwMTMsMjE1MDQyMDU3LDIxMDI1ODIyMzQsMTgz
NjMyNDkyOCwtMTE5NDQ4OTM3NCwtMTg4MTIwNDAyNCwyNzA1ND
UxODQsLTcxODAyMDgxNywtODUxNzk4NDM0LC0xNzgzNTc4MDMz
LC0xOTU0NTU3Mjc4XX0=
-->